#terraform {
#  backend "artifactory" {}
#}

provider "alicloud" {
  region = "cn-shanghai"
}

resource "alicloud_security_group" "ccs_webredirect_ssh_sg_nj" {
  name        = "ccs_webredirect_ssh_sg_nj"
  description = "tf_sg"
}

resource "alicloud_security_group_rule" "ssh-in-nj" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "22/22"
  priority          = 2
  security_group_id = "${alicloud_security_group.ccs_webredirect_ssh_sg_nj.id}"
  cidr_ip           = "0.0.0.0/0"
}

resource "alicloud_security_group_rule" "http-in-nj" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "80/80"
  priority          = 1
  security_group_id = "${alicloud_security_group.ccs_webredirect_ssh_sg_nj.id}"
  cidr_ip           = "0.0.0.0/0"
}

resource "alicloud_instance" "ccs-cn-sha-web-03" {
  availability_zone          = "cn-shanghai-a"
  security_groups            = ["${alicloud_security_group.ccs_webredirect_ssh_sg_nj.id}"]
  instance_type              = "ecs.xn4.small"
  instance_charge_type       = "PostPaid"
  image_id                   = "ubuntu_16_0402_64_20G_alibase_20170818.vhd"
  system_disk_category       = "cloud_efficiency"
  system_disk_size           = 40
  internet_max_bandwidth_in  = 200
  internet_max_bandwidth_out = 10
  instance_name              = "ccs-cn-sha-web-03"
  host_name                  = "ccs-cn-sha-web-03"
  key_name                   = "cn-webredirect-ip"
  user_data                  = "${file("cloud-config.yml")}"
}

resource "alicloud_instance" "ccs-cn-sha-web-04" {
  availability_zone          = "cn-shanghai-b"
  security_groups            = ["${alicloud_security_group.ccs_webredirect_ssh_sg_nj.id}"]
  instance_type              = "ecs.xn4.small"
  instance_charge_type       = "PostPaid"
  image_id                   = "ubuntu_16_0402_64_20G_alibase_20170818.vhd"
  system_disk_category       = "cloud_efficiency"
  system_disk_size           = 40
  internet_max_bandwidth_in  = 200
  internet_max_bandwidth_out = 10
  instance_name              = "ccs-cn-sha-web-04"
  host_name                  = "ccs-cn-sha-web-04"
  key_name                   = "cn-webredirect-ip"
  user_data                  = "${file("cloud-config.yml")}"
}

resource "alicloud_slb" "webredirect-slb-nj" {
  name                 = "ccs-cn-webredirect-slb-nj"
  internet_charge_type = "paybytraffic"
  internet             = "true"
  specification        = "slb.s2.small"
}

resource "alicloud_slb_listener" "webredirect_listener-nj" {
  load_balancer_id          = "${alicloud_slb.webredirect-slb-nj.id}"
  backend_port              = 80
  frontend_port             = 80
  protocol                  = "http"
  bandwidth                 = 5
  health_check_type         = "http"
  health_check_connect_port = 80
  health_check_uri          = "/health"
}

resource "alicloud_slb_attachment" "default-nj" {
  load_balancer_id = "${alicloud_slb.webredirect-slb-nj.id}"
  instance_ids     = ["${alicloud_instance.ccs-cn-sha-web-03.id}", "${alicloud_instance.ccs-cn-sha-web-04.id}"]
}
